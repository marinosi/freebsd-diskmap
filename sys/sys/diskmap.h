/*
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``S IS''AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _D_DISKMAP_H_
#define _D_DISKMAP_H_

#include <sys/queue.h>

#define DISKMAP_API 1
#define MAXNAMELEN 16

#define DISKMAP_SLOT_READY	0x0001U
#define DISKMAP_SLOT_ACTIVE	0x0002U
#define DISKMAP_SLOT_ORPHAN	0x0004U
#define DISKMAP_SLOT_TRACE	0x8000U

/* Lookup information */
struct lut_entry {
	void *vaddr;	/* virtual address */
	vm_paddr_t baddr;	/* Bus address */
	vm_paddr_t paddr;	/* physical address -- in sys/types.h */
};

/* Simple diskmap object unit */
struct diskmap_slot {
	size_t idx;	/* slot index in its pool */
	uint32_t len;	/* length of the slot */
	uint32_t initial_seqno;	/* The initial sequence number carried in this slot */
	uint32_t ofs;	/* Offset within the diskmap buffer. */
	uint16_t flags;	/* XXX IM: revisit this */
	void *metadata;
	struct lut_entry addrinfo; /* expose vaddr & paddr */
	STAILQ_ENTRY(diskmap_slot) diskmap_slots;
};
STAILQ_HEAD(diskmap_slot_queue, diskmap_slot);

/* Buffer pool control block -- variable size */
struct diskmap_bufcb {
	const size_t	buf_ofs;	/* bufpool is always after the bufcb pool */
	char	name[MAXNAMELEN];
	const uint32_t 	num_slots;
	uint32_t	avail;	/* number of available for use buffers */
	uint32_t	cur;	/* current position of the 'ring' */
	uint32_t	reserved;	/* not refilled before current */
	const uint32_t	dr_buf_size;
	/* the diskmap slots follow */
	struct diskmap_slot slot[0]; /* array of slots */
};

/* Diskmap qpair control block struct -- variable size */
struct diskmap_qpaircb {
	const size_t	qpair_ofs;	/* qpair is always after the qpaircb pool */
	const size_t	prp_ofs;	/* qpair is always after the qpaircb pool */
	const uint16_t	num_queues;	/* number of queues */
	const uint16_t	num_slots;	/* number of queues + prp slots */
	const uint32_t	qdepth;		/* qdepth */
	const uint32_t	max_xfer_size;		/* qdepth */
	const uint32_t	qp_id;		/* qpair id */
	const uint32_t	qp_obj_size;	/* qpair obj size */
	const uint32_t	prp_obj_size;	/* prp obj size */
	uint32_t	sq_head;
	uint32_t	sq_tail;
	uint32_t	cq_head;
	uint16_t	qp_phase;		/* Completion queue phase */
	/* Actual slots follow */
	struct diskmap_slot slot[0]; /* array of slots */
};

struct dmreq {
	uint32_t	dr_version;	/* API version */
	size_t	dr_offset;	/* qpaircb or bufcb ofs */
	size_t	dr_offset1;	/* qpaircb or bufcb ofs */
	size_t	dr_memsize;	/* size of the shared mem region */
	char	dr_name[MAXNAMELEN];
	uint16_t	dr_cmd;
	uint32_t	dr_flags;	/* flags */
	uint32_t	dr_reqsz;	/* Req pool size */
	uint32_t	dr_nslots;	/* Number of slots/queues/etc */
	int			dr_devid;	/* controller id */
	uint32_t	dr_qid;		/* queue id */
	uint32_t	dr_qdepth;	/* Queue depth */
	uint32_t	dr_qsize;	/* objsize of each pool */
	uint16_t	dr_arg1;	/* Extra args */
	uint32_t	spare[2];	/* spare space */
};

#define DIOCGINFO	_IOWR('i', 151, struct dmreq)	/* Return diskmap info */
#define DIOCREQBUFPOOL	_IOWR('i', 152, struct dmreq)	/* Request buf pool */
#define DIOCDELBUFPOOL	_IOWR('i', 153, struct dmreq)	/* Request buf pool */
#define DIOCREGQPAIRCB	_IOWR('i', 154, struct dmreq)	/* Register qpair cb */
#define DIOCUNREGQPAIRCB	_IO('i', 155)	/* unregister qpaircb */
#define DIOCSQSYNC	_IO('i', 156)	/* Sync the submission queue */
#define DIOCCQSYNC	_IO('i', 157)	/* Sync the completion queue */

#endif /* _D_DISKMAP_H_ */