/*
 * Copyright (C) 2014 Ilias Marinos.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _DISKMAP_KERN_H_
#define _DISKMAP_KERN_H_

#define DISKMAP_SQ_SYNC	1
#define DISKMAP_CQ_SYNC	2

/*
 * Passed to sync callback
 */
struct sync_args {
	void *drv_qpair_ref;
	uint32_t	sq_head;
	uint32_t	sq_tail;
	uint32_t	cq_head;
};

/*
 * info about the driver qpair to pass to diskmap
 */
struct hw_qpair_info {
	void		*drv_qpair_ref;
	bus_dma_tag_t		dma_tag;
	uint32_t	dev_unit;
	uint32_t	id;
	uint32_t	num_entries;
	uint32_t	sq_head;
	uint32_t	sq_tail;
	uint32_t	cq_head;
	uint16_t	phase;
	uint32_t	max_xfer_size;
	void		**sqptr;
	void		**cqptr;
	void			(*sync)(void *, uint8_t flags);
	void		(*doorbell_info)(void *);
};

int diskmap_attach(struct hw_qpair_info *);
void diskmap_detach(int dev_id, uint32_t id);

#endif /* _DISKMAP_NVME_H_ */
