/*
 * Copyright (C) 2014 Ilias Marinos.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */


#include <sys/types.h>
#include <sys/cdefs.h> /* prerequisite */
__FBSDID("$FreeBSD$");

#include <sys/malloc.h>
#include <sys/proc.h>
#include <vm/vm.h>	/* vtophys */
#include <vm/pmap.h>	/* vtophys */
/*#include <vm/vm_extern.h>*/
/*#include <sys/pcpu.h>*/
/*#include <sys/socket.h> [> sockaddrs <]*/
#include <sys/selinfo.h>
#include <sys/sysctl.h>
#include <sys/lock.h>
#include <sys/mutex.h>
#include <sys/systm.h>
#include <machine/bus.h>	/* bus_dmamap_* */

#include <dev/diskmap/diskmap_private.h>
#include <dev/diskmap/diskmap_mem.h>
#include <sys/diskmap.h>	/* XXX IM: This doesn't fit in net/ */

#define MMIN(a, b) ((a) < (b) ? (a) : (b))
#define MMAX(a, b) ((a) > (b) ? (a) : (b))

#define DISKMAP_BUF_MAX_NUM	40*4096	/* large machine */
#define DISKMAP_POOL_MAX_NAMSZ	32
#define DISKMAP_DEFAULT_QDEPTH	32
#define DISKMAP_CQ_ENTRY_SZ	16	/* CQ entry size */
#define DISKMAP_SQ_ENTRY_SZ	(4*DISKMAP_CQ_ENTRY_SZ)	/* SQ entry size */


enum {
	/*DISKMAP_DEVID_POOL,*/
	DISKMAP_QPAIRCB_POOL = 0,
	DISKMAP_QPAIR_POOL,
	DISKMAP_PRPLIST_POOL,
	DISKMAP_BUFCB_POOL,
	DISKMAP_BUF_POOL,
	DISKMAP_POOLS_NR
};

struct diskmap_obj_params {
	u_int size;
	u_int num;
};

struct diskmap_obj_params diskmap_params[DISKMAP_POOLS_NR] = {
	/*[DISKMAP_DEVID_POOL] = {*/
		/*.size = 1024,*/
		/*.num = 10,*/
	/*},*/
	[DISKMAP_QPAIRCB_POOL] = {
		.size = sizeof(struct diskmap_qpaircb) + 512*sizeof(struct diskmap_slot),
		.num  = 10,
	},
	[DISKMAP_QPAIR_POOL] = {
		.size = 8*PAGE_SIZE,
		.num  = 20,
	},
	[DISKMAP_PRPLIST_POOL] = {
		.size = PAGE_SIZE,
		.num  = 2048,
	},
	[DISKMAP_BUFCB_POOL] = {
		.size = 8*PAGE_SIZE,
		.num = 10,
	},
	[DISKMAP_BUF_POOL] = {
		.size = 3*PAGE_SIZE,
		.num  = DISKMAP_BUF_MAX_NUM/2,
	},
};

struct diskmap_obj_pool {
	char name[DISKMAP_POOL_MAX_NAMSZ];	/* name of the allocator */

	/* ---------------------------------------------------*/
	/* these are only meaningful if the pool is finalized */
	/* (see 'finalized' field in diskmap_mem_d)            */
	u_int objtotal;         /* actual total number of objects. */
	size_t memtotal;		/* actual total memory space */
	u_int objfree;          /* number of free objects. */

	struct lut_entry *lut;  /* virt,phys addresses, objtotal entries */
	uint32_t *bitmap;       /* one bit per buffer, 1 means free */
	uint32_t bitmap_slots;	/* number of uint32 entries in bitmap */
	char *nregion;
	/* ---------------------------------------------------*/

	/* limits */
	u_int objminsize;	/* minimum object size */
	u_int objmaxsize;	/* maximum object size */
	u_int nummin;		/* minimum number of objects */
	u_int nummax;		/* maximum number of objects */

	/* these are changed only by config */
	u_int _objtotal;	/* total number of objects */
	u_int _objsize;		/* object size */

	/* requested values */
	u_int r_objtotal;
	u_int r_objsize;
} __aligned(CACHE_LINE_SIZE);

/* Lock-related types */
#define DMA_LOCK_T		struct mtx

struct diskmap_mem_d {
	DMA_LOCK_T dm_mtx;  /* protect the allocator */
	size_t dm_totalsize; /* shorthand */

	u_int flags;
#define DISKMAP_MEM_FINALIZED	0x1	/* preallocation done */
	int lasterr;		/* last error for curr config */
	int refcount;		/* existing priv structures */
	char *dop_start;	/* Start of allocated region -- placeholder */
	/* the N obj pools*/
	struct diskmap_obj_pool pools[DISKMAP_POOLS_NR];
} __aligned(CACHE_LINE_SIZE);

/*
 * dm_mem is the mem allocator for all NVME disk devices.
 */
struct diskmap_mem_d dm_mem = {	/* Our memory allocator. */
	.pools = {
		/*[DISKMAP_DEVID_POOL] = {*/
			/*.name = "diskmap_devid",*/
			/*.objminsize = sizeof(struct diskmap_devid),*/
			/*.objmaxsize = sizeof(struct diskmap_devid),*/
			/*.nummin     = 1,*/
			/*.nummax	    = 10,*/
		/*},*/
		[DISKMAP_QPAIRCB_POOL] = {
			.name 	= "diskmap_qpaircb",
			.objminsize = sizeof(struct diskmap_qpaircb)+ 2*sizeof(struct diskmap_slot),
			.objmaxsize = sizeof(struct diskmap_qpaircb)+8192*sizeof(struct diskmap_slot),
			.nummin     = 2,	/* don't be stingy */
			.nummax	    = 32,	/* large */
		},
		[DISKMAP_QPAIR_POOL] = {
			.name 	= "diskmap_qpair",
			.objminsize = 64, /* Should be modular */
			.objmaxsize = 512*PAGE_SIZE,
			.nummin     = 2,
			.nummax	    = 64,
		},
		[DISKMAP_PRPLIST_POOL] = {
			.name 	= "diskmap_prp",
			.objminsize = 64,
			.objmaxsize = PAGE_SIZE,
			.nummin     = 1,
			.nummax	    = 8192,
		},
		[DISKMAP_BUFCB_POOL] = {
			.name = "diskmap_bufcb",
			.objminsize = sizeof(struct diskmap_bufcb)+sizeof(struct diskmap_slot),
			.objmaxsize = sizeof(struct diskmap_bufcb)+100000*sizeof(struct diskmap_slot),
			.nummin     = 1,
			.nummax	    = 100000,
		},
		[DISKMAP_BUF_POOL] = {
			.name	= "diskmap_buf",
			.objminsize = 64,
			.objmaxsize = 131200,
			.nummin     = 4,
			.nummax	    = 50000000, /* fifty millions! */
		},
	},
};

/* Lock-related macros */
/*#define DMA_LOCK_INIT()		mtx_init(&dm_mem.dm_mtx, "diskmap memory allocator lock", NULL, MTX_DEF)*/
/*#define DMA_LOCK_DESTROY()	mtx_destroy(&dm_mem.dm_mtx)*/
/*#define DMA_LOCK()		mtx_lock(&dm_mem.dm_mtx)*/
/*#define DMA_UNLOCK()		mtx_unlock(&dm_mem.dm_mtx)*/

#define DMA_LOCK_INIT()		DM_LOCK_INIT(dm_mem.dm_mtx, "diskmap memory allocator lock")
#define DMA_LOCK_DESTROY()	DM_LOCK_DESTROY(dm_mem.dm_mtx)
#define DMA_LOCK()			DM_LOCK(dm_mem.dm_mtx)
#define DMA_UNLOCK()		DM_UNLOCK(dm_mem.dm_mtx)

#if 0
/* accessor functions */
struct lut_entry*
diskmap_mem_get_lut(struct diskmap_mem_d *dmd)
{
	return dmd->pools[DISKMAP_BUF_POOL].lut;
}

u_int
diskmap_mem_get_buftotal(struct diskmap_mem_d *dmd)
{
	return dmd->pools[DISKMAP_BUF_POOL].objtotal;
}

size_t
diskmap_mem_get_bufsize(struct diskmap_mem_d *dmd)
{
	return dmd->pools[DISKMAP_BUF_POOL]._objsize;
}
#endif


/* memory allocator related sysctls */
#define STRINGIFY(x) #x


#define DECLARE_SYSCTLS(id, name) \
    SYSCTL_INT(_dev_diskmap, OID_AUTO, name##_size, \
        CTLFLAG_RW, &diskmap_params[id].size, 0, "Requested size of diskmap " STRINGIFY(name) "s"); \
    SYSCTL_INT(_dev_diskmap, OID_AUTO, name##_curr_size, \
        CTLFLAG_RD, &dm_mem.pools[id]._objsize, 0, "Current size of diskmap " STRINGIFY(name) "s"); \
    SYSCTL_INT(_dev_diskmap, OID_AUTO, name##_num, \
        CTLFLAG_RW, &diskmap_params[id].num, 0, "Requested number of diskmap " STRINGIFY(name) "s"); \
    SYSCTL_INT(_dev_diskmap, OID_AUTO, name##_curr_num, \
        CTLFLAG_RD, &dm_mem.pools[id].objtotal, 0, "Current number of diskmap " STRINGIFY(name) "s")

/*#define DECLARE_SYSCTLS(id, name) \*/
	/*SYSCTL_INT(_dev_diskmap, OID_AUTO, name##_size, \*/
		/*CTLFLAG_RW, &diskmap_params[id].size, 0, "Requested size of diskmap " STRINGIFY(name) "s")*/

SYSCTL_DECL(_dev_diskmap);
DECLARE_SYSCTLS(DISKMAP_QPAIRCB_POOL, qpaircb);
DECLARE_SYSCTLS(DISKMAP_QPAIR_POOL, qpair);
DECLARE_SYSCTLS(DISKMAP_PRPLIST_POOL, prplist);
DECLARE_SYSCTLS(DISKMAP_BUFCB_POOL, bufcb);
DECLARE_SYSCTLS(DISKMAP_BUF_POOL, buf);

/*
 * Called with DMA_LOCK held!
 */
static void
diskmap_reset_obj_allocator(void)
{
	u_int i;
	struct diskmap_obj_pool *p;

	for (i=0; i < DISKMAP_POOLS_NR; i++) {
		p = &dm_mem.pools[i];
		p->nregion = NULL;
		if (p->lut != NULL) {
			bzero(p->lut, sizeof(struct lut_entry) * p->objtotal);
			free(p->lut, M_DISKMAP);
			p->lut = NULL;
		}
		if (p->bitmap != NULL) {
			free(p->bitmap, M_DISKMAP);
			p->bitmap = NULL;
		}
		p->objtotal = 0;
		p->objfree = 0;
	}

	/* Free the diskmap mem region */
	if (dm_mem.dop_start != NULL) {
		contigfree(dm_mem.dop_start, dm_mem.dm_totalsize, M_DISKMAP);
		D("Released diskmap memory (%luKB)", dm_mem.dm_totalsize >> 10);
		dm_mem.dop_start = NULL;
		dm_mem.dm_totalsize = 0;
	}
}

static int
diskmap_obj_allocator(void)
{
	int err = 0;
	size_t i, n, q, totalsize;
	struct diskmap_obj_pool *p;
	char *nregion = NULL;

	totalsize = 0;
	for (q = 0; q < DISKMAP_POOLS_NR; q++) {
		/* Align all object pools to PAGE_SIZE boundary */
		p = &dm_mem.pools[q];
		p->memtotal = m_align((size_t)p->_objsize * p->objtotal, PAGE_SIZE);
		totalsize += p->memtotal;
	}


	if (totalsize == 0)
		return EINVAL;

	nregion = contigmalloc(totalsize, M_DISKMAP, M_NOWAIT | M_ZERO, 0, -1UL,
		PAGE_SIZE, 0);
	if(nregion == NULL) {
		D("Unable to create diskmap memory region (size: %lu)", totalsize);
		return ENOMEM;
	}
	dm_mem.dop_start = nregion;
	dm_mem.dm_totalsize = totalsize;

	D("Pre-allocated %luKB for diskmap memory", totalsize >> 10);

	char *pool = nregion;
	for (q=0; q < DISKMAP_POOLS_NR; q++) {
		p = &dm_mem.pools[q];
		p->nregion = pool;

		/* Allocate the lookup table */
		n = sizeof(struct lut_entry) * p->objtotal;
		p->lut = malloc(n, M_DISKMAP, M_NOWAIT | M_ZERO);
		if (p->lut == NULL) {
			D("Unable to create lookup table (%zu bytes) for '%s'", n, p->name);
			err = ENOMEM;
			goto clean;
		}

		/* Allocate the bitmap */
		n = (p->objtotal + 31)/32;
		p->bitmap = malloc(sizeof(uint32_t)*n, M_DISKMAP, M_NOWAIT | M_ZERO);
		if (p->bitmap == NULL) {
			D("Unable to create lookup table (%zu bytes) for '%s'", n, p->name);
			err = ENOMEM;
			goto clean;
		}

		p->bitmap_slots = n;
		D("Bitmap slots: %zu", n);

		/*KASSERT(kernel_pmap == pmap, ("pmap is not kernel pmap"));*/
		/*if (kernel_pmap != pmap)*/
			/*D("pmap is not kernel pmap");*/

		/* Iterate through all objects of the diskmap_obj_pool */
		for (i=0; i < p->objtotal; i++ ) {
			char *objstart = pool + (size_t) i * p->_objsize;
			/*
			 * Check if we run out of region -- shouldn't happen
			 */
			if (objstart + p->_objsize > pool + p->memtotal) {
				err = EFAULT; /* XXX IM: revisit error code */
				goto clean;
			}

			p->bitmap[ (i>>5) ] |= (1 << (i & 31));
			p->lut[i].vaddr = objstart;
			p->lut[i].baddr = p->lut[i].paddr = vtophys(objstart);
			if ( i==0 || ((i+1) == p->objtotal))
				D("Object [%zu] -- vaddr: %p, paddr: %lx, paddr2: %lx", i,
					p->lut[i].vaddr, p->lut[i].paddr,
					pmap_kextract((vm_offset_t) objstart));
		}

		/* Update pool-specific values */
		p->objfree = p->objtotal;

		/* Advance pool to point to the next diskmap_obj_pool */
		pool += p->memtotal;
		if (pool > dm_mem.dop_start + dm_mem.dm_totalsize) {
			D("Oops, pool '%p' is out-of-range (boundary: '%p')", pool, (char *)
				dm_mem.dop_start + dm_mem.dm_totalsize);
			err = EFAULT; /* XXX IM: revisit error code */
			goto clean;
		}
	}

	return 0;

clean:
	diskmap_reset_obj_allocator();
	return err;
}

/*#define CACHE_LINE_SIZE 64*/
static int
diskmap_config_obj_allocator(struct diskmap_obj_pool *p, u_int objtotal, u_int
	objsize)
{

#if 0
	if (p == &dm_mem.pools[DISKMAP_BUF_POOL])
		objsize = round2np2((uint16_t) objsize);
	else
#endif
		/* Align the objsize to CACHE_LINE_SIZE boundaries */
		objsize = m_align(objsize, CACHE_LINE_SIZE);

	if (objsize < p->objminsize || objsize > p->objmaxsize) {
		D("requested objsize %u is out of range [%u, %u]", objsize,
			p->objminsize, p->objmaxsize);
		return EINVAL;
	}

	if (objtotal < p->nummin || objtotal > p->nummax) {
		D("requested objtotal %u is out of range [%u, %u]", objtotal,
			p->nummin, p->nummax);
		return EINVAL;
	}

	D("Pool [%s] old objsize: %u, old objtotal: %u", p->name, p->_objsize,
		p->objtotal);
	p->_objsize = objsize;
	p->objtotal = objtotal;

	D("Pool [%s] new objsize: %u, new objtotal: %u", p->name, p->_objsize,
		p->objtotal);

	/*
	 * p->memtotal is set in diskmap_obj_allocator() after is aligned to
	 * PAGE_SIZE boundaries -- no need to touch it here.
	 * */
	return 0; /* Everything is fine */
}

static int
diskmap_mem_config_changed(void)
{
    int i;
	for (i = 0; i < DISKMAP_POOLS_NR; i++) {
		if (dm_mem.pools[i]._objsize != diskmap_params[i].size ||
		    dm_mem.pools[i].objtotal != diskmap_params[i].num)
		    return 1;
	}
	return 0;
}

static int
diskmap_mem_config(void)
{
	u_int i;

	/* If it is already used go to out */
	ND("dm_mem refcount: %u", dm_mem.refcount);
	if (dm_mem.refcount > 1) {
		D("diskmap memory is used, won't reconfigure");
		goto out;
	}

	if(!diskmap_mem_config_changed())
		goto out;

	D("Reconfiguring diskmap memory");

	if(dm_mem.flags & DISKMAP_MEM_FINALIZED) {
		diskmap_reset_obj_allocator();
		dm_mem.flags &= ~DISKMAP_MEM_FINALIZED;
	}

	for (i = 0; i < DISKMAP_POOLS_NR; i++) {
		dm_mem.lasterr = diskmap_config_obj_allocator(&dm_mem.pools[i],
			diskmap_params[i].num, diskmap_params[i].size);
		if (dm_mem.lasterr)
			goto out;
	}

	D("Have %lu KB for bufcb blocks, have %lu KB for qpaircb blocks, "
		"%lu KB for qpairs, %lu KB for prplists, and %lu MB for buffers",
		dm_mem.pools[DISKMAP_BUFCB_POOL].memtotal >> 10,
		dm_mem.pools[DISKMAP_QPAIRCB_POOL].memtotal >> 10,
		dm_mem.pools[DISKMAP_QPAIR_POOL].memtotal >> 10,
		dm_mem.pools[DISKMAP_PRPLIST_POOL].memtotal >> 10,
		dm_mem.pools[DISKMAP_BUF_POOL].memtotal >> 20);

out:
	return dm_mem.lasterr;
}

vm_paddr_t
diskmap_mem_ofstophys(vm_ooffset_t offset)
{
	u_int i;
	vm_ooffset_t o = offset;
	vm_paddr_t pa;
	struct diskmap_obj_pool *p;

	DMA_LOCK();
	if (!(dm_mem.flags & DISKMAP_MEM_FINALIZED)) {
		DMA_UNLOCK();
		D("Diskmap mem is not finalized!");
		return 0;
	}

	p = dm_mem.pools;
	for (i = 0 ; i < DISKMAP_POOLS_NR; offset -= p[i].memtotal, i++) {
		if (offset >= p[i].memtotal)
			continue;
		pa = p[i].lut[offset/p[i]._objsize].paddr + offset % p[i]._objsize;
		DMA_UNLOCK();
		return pa;
	}
	/*
	 * Not quite right, since every diskmap pool is aligned to PAGE_SIZE
	 * boundaries but the actual objects may not use all the allocated region
	 * (subject to objtotal & objsize)
	 */
	D("invalid ofs 0x%lx out of [0, 0x%lx)", (size_t) o, dm_mem.dm_totalsize);

	DMA_UNLOCK();
	return 0; /* Bad address */
}

/*
 * This method gets as arguments a diskmap object pool, and an object address
 * and it returns the offset from the start (base vaddr) of the pool.
 * If the object doesn't belong in this pool, it returns 0.
 */
static size_t
diskmap_obj_offset(struct diskmap_obj_pool *p, const void *vaddr)
{

	char *objvaddr = __DECONST(char *, vaddr);

	/* Out-of-range */
	if ( objvaddr < p->nregion || objvaddr > (p->nregion + p->_objsize
		* p->objtotal)) {
		D("address %p not contained in pool [%s]", vaddr, p->name);
		return 0; /* Bad address */
	}

	return (objvaddr - p->nregion);
}

/*
 * Calculate the distance of "obj" from "from"
 * can be negative
 */
static ssize_t
diskmap_obj_rel_offset(const void *from, const void *obj)
{
	char *ofrom = __DECONST(char *, from);
	char *oobj = __DECONST(char *, obj);

	/* Check if from or obj are out of range */
	if ( oobj < dm_mem.dop_start || oobj > dm_mem.dop_start
		+ dm_mem.dm_totalsize) {
		D("address %p not contained in whole diskmap mem region", oobj );
		return 0; /* Bad address */
	}

	if( ofrom < dm_mem.dop_start || ofrom > dm_mem.dop_start
		+ dm_mem.dm_totalsize) {
		D("address %p not contained in whole diskmap mem region", ofrom);
		return 0; /* Bad address */
	}

	return (oobj - ofrom);
}

/* Helper functions which convert virtual addresses to offsets */
#define diskmap_qpaircb_abs_offset(v)					\
	diskmap_obj_offset(&dm_mem.pools[DISKMAP_QPAIRCB_POOL], (v))

#define diskmap_qpair_abs_offset(v)					\
    (dm_mem.pools[DISKMAP_QPAIRCB_POOL].memtotal + 		\
	diskmap_obj_offset(&dm_mem.pools[DISKMAP_QPAIR_POOL], (v)))

#define diskmap_prplist_abs_offset(v)					\
    (dm_mem.pools[DISKMAP_QPAIRCB_POOL].memtotal +		\
	dm_mem.pools[DISKMAP_QPAIR_POOL].memtotal + 				\
	diskmap_obj_offset(&dm_mem.pools[DISKMAP_PRPLIST_POOL], (v)))

#define diskmap_bufcb_abs_offset(v)					\
	(dm_mem.pools[DISKMAP_QPAIRCB_POOL].memtotal +		\
	dm_mem.pools[DISKMAP_QPAIR_POOL].memtotal + 				\
	dm_mem.pools[DISKMAP_PRPLIST_POOL].memtotal +		\
	diskmap_obj_offset(&dm_mem.pools[DISKMAP_BUFCB_POOL], (v)))

#define diskmap_buf_abs_offset(v)					\
	(dm_mem.pools[DISKMAP_QPAIRCB_POOL].memtotal +		\
	dm_mem.pools[DISKMAP_QPAIR_POOL].memtotal + 				\
	dm_mem.pools[DISKMAP_PRPLIST_POOL].memtotal +		\
	dm_mem.pools[DISKMAP_BUFCB_POOL].memtotal +		\
	diskmap_obj_offset(&dm_mem.pools[DISKMAP_BUF_POOL], (v)))

/* id is the objpool index (e.g., DISKMAP_BUFCB_POOL) */
#define diskmap_rel_offset(id, v)					\
	diskmap_obj_offset(&dm_mem.pools[id], (v))

static void *
diskmap_obj_malloc(struct diskmap_obj_pool *p, u_int len, uint32_t *start,
    uint32_t *index)
{
	uint32_t i = 0;	/* bitmap index */
	uint32_t mask, j;	/* slotv counter */
	void *vaddr = NULL;

	if ( len > p->_objsize) {
		D("%s request size %d too large", p->name, len);
		return NULL;
	}

	if (p->objfree == 0) {
		D("no more %s objects", p->name);
		return NULL;
	}

	if (start)
		i = *start;

	/* termination is guaranteed by p->free, but better check bounds on i */
	while (vaddr == NULL && i < p->bitmap_slots)  {
		uint32_t cur = p->bitmap[i];
		if (cur == 0) { /* bitmask is fully used */
			i++;
			continue;
		}
		/* locate a slot */
		for (j = 0, mask = 1; (cur & mask) == 0; j++, mask <<= 1)
			;

		p->bitmap[i] &= ~mask; /* mark object as in use */
		p->objfree--;

		vaddr = p->lut[i * 32 + j].vaddr;
		if (index)
			*index = i * 32 + j;
	}
	ND("%s allocator: allocated object @ [%d][%d]: vaddr %p", i, j, vaddr);

	if (start)
		*start = i;
	return vaddr;
}

/*
 * free by index, not by address.
 * XXX should we also cleanup the content ?
 */
static int
diskmap_obj_free(struct diskmap_obj_pool *p, uint32_t j)
{
	uint32_t *ptr, mask;

	/* XXX IM: Check if mem is finalized */

	if (j >= p->objtotal) {
		D("invalid index %u, max %u", j, p->objtotal);
		return 1;
	}
	ptr = &p->bitmap[j / 32];
	mask = (1 << (j % 32));
	if (*ptr & mask) {
		D("ouch, double free on buffer %d", j);
		return 1;
	} else {
		*ptr |= mask;
		p->objfree++;
		return 0;
	}
}

/*
 * free by address. This is slow but is only used for a few
 * objects (rings, nifp)
 */
static void
diskmap_obj_free_va(struct diskmap_obj_pool *p, void *vaddr)
{
	size_t relofs;
	uint32_t index;

	/* XXX IM: Check if mem is finalized */

	/*
	 * XXX IM: Fix this instead of p->memtotal it should be objtot * objsize
	 */
	if ((char *)vaddr < (char *) p->nregion || (char *) vaddr > (char
		*)p->nregion + p->memtotal) {
		D("address %p is not contained inside the pool (%s)",
			vaddr, p->name);
		return;
	}

	bzero(vaddr, p->_objsize);
	relofs = (char *)vaddr - (char *)p->nregion;

	index = relofs/p->_objsize;	/* This will round to the start of the obj */
	diskmap_obj_free(p, index);
}

#define diskmap_mem_bufsize()	\
	((dm_mem).pools[DISKMAP_BUF_POOL]._objsize)
#define diskmap_mem_prpsize()	\
	((dm_mem).pools[DISKMAP_PRPLIST_POOL]._objsize)

#define diskmap_qpaircb_malloc(len)	diskmap_obj_malloc(&dm_mem.pools[DISKMAP_QPAIRCB_POOL], len, NULL, NULL)
#define diskmap_qpaircb_free(v)		diskmap_obj_free_va(&dm_mem.pools[DISKMAP_QPAIRCB_POOL], (v))
#define diskmap_qpair_malloc(len, _pos, _index)		\
	diskmap_obj_malloc(&dm_mem.pools[DISKMAP_QPAIR_POOL], len, _pos, _index)
#define diskmap_qpair_free(v)		diskmap_obj_free_va(&dm_mem.pools[DISKMAP_QPAIR_POOL], (v))
#define diskmap_prplist_malloc(_pos, _index)		\
	diskmap_obj_malloc(&dm_mem.pools[DISKMAP_PRPLIST_POOL], diskmap_mem_prpsize(), _pos, _index)
#define diskmap_prplist_free(v)		diskmap_obj_free_va(&dm_mem.pools[DISKMAP_PRPLIST_POOL], (v))
#define diskmap_bufcb_malloc(len)	diskmap_obj_malloc(&dm_mem.pools[DISKMAP_BUFCB_POOL], len, NULL, NULL)
#define diskmap_bufcb_free(v)		diskmap_obj_free_va(&dm_mem.pools[DISKMAP_BUFCB_POOL], (v))
#define diskmap_buf_malloc(_pos, _index)			\
	diskmap_obj_malloc(&dm_mem.pools[DISKMAP_BUF_POOL], diskmap_mem_bufsize(), _pos, _index)

static struct diskmap_bufcb *
find_bufcb(const char *name)
{
	uint32_t index;
	struct diskmap_obj_pool *p = &dm_mem.pools[DISKMAP_BUFCB_POOL];
	struct diskmap_bufcb *bcb;

	D("Looking for [%s] name identifier", name);

	for (index = 0; index < p->objtotal; index++) {
			if (p->bitmap[ (index>>5) ] & (1 << (index & 31)))
				continue;

			bcb = (struct diskmap_bufcb *)((char *)p->nregion + index
				* p->_objsize);
			ND("Cur bufcb name: %s", bcb->name);
			if (!strcmp(bcb->name, name)) {
				D("Bufpool found");
				return bcb;
			}
	}
	/* Not found */
	return NULL;
}

/* Return nonzero on error */
static int
diskmap_alloc_bufs(struct diskmap_slot *slot, u_int n)
{
	struct diskmap_obj_pool *p = &dm_mem.pools[DISKMAP_BUF_POOL];
	u_int i = 0;	/* slot counter */
	uint32_t pos = 0;	/* slot in p->bitmap */
	uint32_t index = 0;	/* buffer index */

	for (i = 0; i < n; i++) {
		void *vaddr = diskmap_buf_malloc(&pos, &index);
		if (vaddr == NULL) {
			D("unable to locate empty packet buffer");
			goto cleanup;
		}
		slot[i].idx = index;
		slot[i].len = p->_objsize;
		slot[i].addrinfo.vaddr = p->lut[index].vaddr;
		slot[i].addrinfo.baddr = slot[i].addrinfo.paddr = p->lut[index].paddr;

		if (i == 0 || i == n-1)
			D("Buffer [%u] -- vaddr: %p, paddr: %lx, paddr2: %lx", i,
				slot[i].addrinfo.vaddr, slot[i].addrinfo.paddr,
				pmap_kextract((vm_offset_t) vaddr));
	}

	D("allocated %d buffers, %d available, first at %d", n, p->objfree, pos);
	return (0);

cleanup:
	while (i > 0) {
		i--;
		diskmap_obj_free(p, slot[i].idx);
	}
	bzero(slot, n * sizeof(slot[0]));
	return (ENOMEM);
}

static void
diskmap_free_buf(uint32_t i)
{
	struct diskmap_obj_pool *p = &dm_mem.pools[DISKMAP_BUF_POOL];

	if (i < 2 || i >= p->objtotal) {
		D("Cannot free buf#%d: should be in [2, %d[", i, p->objtotal);
		return;
	}
	diskmap_obj_free(p, i);
}

/*
 * Called with DMA_LOCK_HELD
 * Main method to allocate all diskmap-related memory
 */
static int
diskmap_mem_allocate_locked(void)
{
	int i;
	int err;

	dm_mem.refcount++;
	if (dm_mem.refcount > 1) {
		D("busy (refcount %d)", dm_mem.refcount);
		goto out;
	}

	/* update configuration if changed */
	if (diskmap_mem_config())
		goto out;

	if (dm_mem.flags & DISKMAP_MEM_FINALIZED) {
		D("Finalized, nothing to do");
		goto out;
	}

	/* Bump reference count */
	ND("dm_mem.refcount: %d", (int) dm_mem.refcount);

	dm_mem.lasterr = diskmap_obj_allocator();
	if (dm_mem.lasterr)
		goto cleanup;

	/* make sysctl values match actual values in the pools */
	for (i = 0; i < DISKMAP_POOLS_NR; i++) {
		diskmap_params[i].size = dm_mem.pools[i]._objsize;
		diskmap_params[i].num  = dm_mem.pools[i].objtotal;
	}

	/*
	 * Specific to the BUF_POOL -- reserve the first 2 buffers
	 * XXX IM: Revisit this.
	 */
	dm_mem.pools[DISKMAP_BUF_POOL].objfree -= 2;
	dm_mem.pools[DISKMAP_BUF_POOL].bitmap[0] = ~3;

	dm_mem.flags |= DISKMAP_MEM_FINALIZED;
	dm_mem.lasterr = 0;
	goto unlock_out;

cleanup:
	diskmap_reset_obj_allocator();	/* doesn't touch dm_mem.lasterr */

out:
	if(dm_mem.lasterr)
		dm_mem.refcount--;

unlock_out:
	D("==> dm_mem.refcount: %d", (int) dm_mem.refcount);
	err = dm_mem.lasterr;
	return err;
}

int
diskmap_mem_allocate(void)
{
    int err;

	DMA_LOCK();
	err = diskmap_mem_allocate_locked();
	DMA_UNLOCK();

	return err;
}

int
diskmap_mem_get_info(size_t *size, u_int *memflags)
{
	int err = 0;
	DMA_LOCK();

	if (dm_mem.refcount == 0) {
		err = diskmap_mem_config();
		if (err)
			goto out;
	}

	if (size) {
		if (dm_mem.flags & DISKMAP_MEM_FINALIZED) {
			*size = dm_mem.dm_totalsize;
		} else {
			int i;
			*size = 0;
			for (i=0; i < DISKMAP_POOLS_NR; i++) {
				struct diskmap_obj_pool *p = &dm_mem.pools[i];
				*size += m_align((p->objtotal * p->_objsize), PAGE_SIZE);
			}
		}
	}
	if (memflags)
		*memflags = dm_mem.flags;

out:
	DMA_UNLOCK();
	return err;
}

/* call with DMA_LOCK held */
void
diskmap_mem_deref(void)
{
	DMA_LOCK();
	D("Dereferencing memory -- old refcount: %u", dm_mem.refcount);
	if (dm_mem.refcount >= 1)
		dm_mem.refcount--;
	else
		D("dm_mem refcount -- tried to deref unreferenced");

	if (diskmap_verbose)
		D("dm_mem refcount = %d", dm_mem.refcount);
	D("Dereferencing memory -- new refcount: %u", dm_mem.refcount);
	DMA_UNLOCK();
}

int
diskmap_mem_init(void)
{
	DMA_LOCK_INIT();
	return 0;
}

void
diskmap_mem_terminate(void)
{
	DMA_LOCK();
	/* Free allocated memory */
	diskmap_reset_obj_allocator();
	DMA_UNLOCK();

	/*
	 * XXX IM: This seems unsafe since the lock could be held again before
	 * destroyed?
	 */

	/* Destroy lock */
	DMA_LOCK_DESTROY();

}

size_t
diskmap_get_totalmem(void)
{
	size_t sz;
	DMA_LOCK();

	/* XXX IM --  check if mem is finalized? */
	sz = dm_mem.dm_totalsize;
	DMA_UNLOCK();

	return sz;
}

/*
 * nsize should be aligned to BUF_SIZE before
 */
void *
diskmap_bufcb_new(u_int nsize, const char *name, size_t *ofs, uint32_t *nnslots)
{
	struct diskmap_bufcb *bcb = NULL;
	u_int nslots, len;
	struct diskmap_obj_pool *p = dm_mem.pools;
	int err;

	/* Name shouldn't be empty, size shouldn't be 0 */
	if (name[0] == '\0' || nsize == 0)
		return NULL;

	DMA_LOCK();

	if (dm_mem.flags & ~DISKMAP_MEM_FINALIZED) {
		D("Diskmap memory is not finalized!");
		goto fail;
	}

	if (find_bufcb(name)) {
		D("Bufcb with this name already exists");
		goto fail;
	}

	/* Align the requested size to BUF_SIZE boundaries */
	nsize = m_align(nsize, diskmap_mem_bufsize());

	/* Find out how many bufslots are needed. */
	nslots = nsize/diskmap_mem_bufsize();

	/*
	 * Calculate the length we'll need to allocate and try to allocate.
	 */
	len = sizeof(struct diskmap_bufcb) + nslots * sizeof(struct diskmap_slot);
	bcb = diskmap_bufcb_malloc(len);
	if (bcb == NULL) {
		D("Cannot allocate buffer control block");
		goto fail;
	}

	/* Allocate all the buffers needed */
	err = diskmap_alloc_bufs(bcb->slot, nslots);
	if (err)
		goto fail_bufs;

	/* Copy the name identifier */
	strncpy(bcb->name, name, MAXNAMELEN);

	/* Calculate buf_ofs. */
	*(ssize_t *)(uintptr_t)&bcb->buf_ofs =
		diskmap_obj_rel_offset(bcb, p[DISKMAP_BUF_POOL].nregion);
		/*(p[DISKMAP_BUFCB_POOL]._memtotal -*/
		/*diskmap_rel_offset(DISKMAP_BUFCB_POOL, bcb);*/

	/* Set the number of buffers in this pool. */
	*(uint32_t *)(uintptr_t)&bcb->num_slots = nslots;

	/* Initialize some variables. */
	bcb->avail = nslots;
	bcb->cur = bcb->reserved = 0;
	*(uint32_t *)(uintptr_t)&bcb->dr_buf_size = diskmap_mem_bufsize();

	/* Update the offset for the newly allocated object */
	if(ofs)
		*ofs = diskmap_bufcb_abs_offset((void *)bcb);

	/* Update the number of slots allocated */
	if(nnslots)
		*nnslots = nslots;

	if (diskmap_verbose) {
		D("Buffer pool with %d buffers has been allocated!", nslots);
		D("Remaining %u unused object for buffer control blocks",
			dm_mem.pools[DISKMAP_BUFCB_POOL].objfree);
	}

	DMA_UNLOCK();
	return bcb;

fail_bufs:
	D("Cannot allocate buffers for the new pool");
	diskmap_bufcb_free(bcb);
fail:
	DMA_UNLOCK();
	return NULL;
}

/*
 * Destroy a buffer pool control block
 */
void
diskmap_bufcb_destroy(const char *name)
{
	u_int i;
	struct diskmap_bufcb *bcb = NULL;

	DMA_LOCK();

	if (!(dm_mem.flags & DISKMAP_MEM_FINALIZED)) {
		DMA_UNLOCK();
		return;
	}

	bcb = find_bufcb(name);
	if (bcb != NULL) {
		/* if not found return */
		D("Buffer pool %s does not exist", name);
		DMA_UNLOCK();
		return;
	}

	/* Release buffers */
	for (i = 0; i < bcb->num_slots; i++)
		diskmap_free_buf(bcb->slot[i].idx);

	/* zero it out */
	bzero(bcb, sizeof(*bcb));

	/* free */
	diskmap_bufcb_free(bcb);

	DMA_UNLOCK();
}

void
diskmap_bufcb_destroy1(struct diskmap_bufcb *bcb)
{
	int i;

	KASSERT(bcb != NULL, ("bcb is not initialized"));

	DMA_LOCK();
	/* Release buffers */
	for (i = 0; i < bcb->num_slots; i++)
		diskmap_free_buf(bcb->slot[i].idx);

	/* zero it out */
	bzero(bcb, sizeof(*bcb));

	/* free */
	diskmap_bufcb_free(bcb);

	DMA_UNLOCK();
}

static void
diskmap_dmar_load_map_cb(void *arg, bus_dma_segment_t *seg, int nseg, int error
	__unused)
{
	uint64_t *bus_addr = (uint64_t *) arg;

	if (error != 0)
		D("dmamap_load err %d\n", error);
	ND("nsegs: %u, seg addr: 0x%lx", nseg, seg[0].ds_addr);
	*bus_addr = seg[0].ds_addr;
}

static int
diskmap_alloc_qpair(struct diskmap_kern_qpair *kqpr, struct diskmap_slot *slot,
	uint16_t nqueues, uint32_t qdepth)
{
	struct diskmap_obj_pool *qp = &dm_mem.pools[DISKMAP_QPAIR_POOL];
	struct diskmap_obj_pool *prp = &dm_mem.pools[DISKMAP_PRPLIST_POOL];
	u_int i = 0;	/* slot counter */
	uint32_t pos = 0;	/* slot in p->bitmap */
	uint32_t index = 0;	/* buffer index */
	uint32_t sq_sz, cq_sz, sz;

	if (qdepth < 2)
		return -1;

	if (qdepth * DISKMAP_SQ_ENTRY_SZ > qp->_objsize) {
		D("Requested qdepth (%u) requires object of size larger than (%u)",
			qdepth, qp->_objsize);
		return ENOMEM;
	}

	sq_sz = qdepth * DISKMAP_SQ_ENTRY_SZ;
	cq_sz = qdepth * DISKMAP_CQ_ENTRY_SZ;

	for (i = 0; i < nqueues; i++) {
		if ( i & 1)
			sz = cq_sz;
		else
			sz = sq_sz;

		void *vaddr = diskmap_qpair_malloc(sz, &pos, &index);
		if (vaddr == NULL) {
			D("unable to locate unused qpair object");
			goto cleanup;
		}
		slot[i].idx = index;
		slot[i].len = sz;
		slot[i].addrinfo.vaddr = qp->lut[index].vaddr;
		slot[i].addrinfo.baddr = slot[i].addrinfo.paddr = qp->lut[index].paddr;
		D("Allocated qpair object phys@%lx, virt@%p with size: %u",
			qp->lut[index].paddr, qp->lut[index].vaddr, sz);

		/* XXX IM: ugly hack -- make that nice.. */
		if (i & 1)
			kqpr->cqueue = qp->lut[index].vaddr;
		else
			kqpr->squeue = qp->lut[index].vaddr;
	}
	*(uint32_t *)(uintptr_t)&kqpr->qp_obj_size = qp->_objsize;
	D("allocated %hu qpairs, %d available, first at %d", nqueues, qp->objfree,
		pos);

	pos = index = 0;
	for (; i < nqueues + qdepth; i++) {
		void *vaddr = diskmap_prplist_malloc(&pos, &index);
		if(vaddr == NULL) {
			D("unable to locate unused prplist object");
			goto cleanup;
		}
		slot[i].idx = index;
		slot[i].addrinfo.vaddr = prp->lut[index].vaddr;
		slot[i].addrinfo.baddr = slot[i].addrinfo.paddr = prp->lut[index].paddr;

		bus_dmamap_load(kqpr->dma_tag, kqpr->prp_dma_map[i-nqueues],
			prp->lut[index].vaddr, prp->_objsize, diskmap_dmar_load_map_cb,
			&slot[i].addrinfo.baddr, 0);
	}
	D("Allocated %u prp objects, %d available, last at %d", qdepth,
		prp->objfree, pos);

	return 0;

cleanup:
	while (i > 0) {
		i--;
		if (i < nqueues)
			diskmap_obj_free(qp, slot[i].idx);
		else
			diskmap_obj_free(prp, slot[i].idx);
	}
	bzero(slot, (nqueues + qdepth) * sizeof(slot[0]));
	return ENOMEM;
}

struct diskmap_qpaircb *
diskmap_qpaircb_new(struct diskmap_kern_qpair *kqpr, uint32_t id, uint32_t
	qdepth, size_t *ofs)
{
	int err;
	u_int len;
	struct diskmap_qpaircb *qcb = NULL;
	uint16_t num_queues = 2;
	struct diskmap_obj_pool *p = dm_mem.pools;

	/*
	 * Qdepth should be checked against controller's capabilities
	 */
	/*if(depth)*/
		/*num_entries = round2np2(*depth);*/
	if (qdepth < 2 || qdepth > 32768) {
		D("Problem");
		return NULL;
	}

	/*D("normalised qdepth: %hu", num_entries);*/
	DMA_LOCK();

	/*
	 * we assume a qpair (1 SQ - 1 CQ) and this is why "2" (num_queues) is
	 * hardcoded here.
	 * NVMe devices support N:1 for SQ:CQ so we should check on this in the
	 * future
	 */
	len = sizeof(struct diskmap_qpaircb) + (num_queues + qdepth) * sizeof(struct
		diskmap_slot);
	qcb = diskmap_qpaircb_malloc(len);
	if (qcb == NULL) {
		D("Failed to allocate qpair control block");
		goto fail;
	}

	/*
	 * allocate the qpairs
	 */
	err = diskmap_alloc_qpair(kqpr, qcb->slot, num_queues, qdepth);
	if (err) {
		D("Failed to allocate qpairs");
		goto fail_qpair;
	}

	/* We need to get the queue id here */
	*(uint32_t *)(uintptr_t)&qcb->qp_id = id;

	/* Calculate the qpair pool offset */
	*(ssize_t *)(uintptr_t)&qcb->qpair_ofs =
		diskmap_obj_rel_offset(qcb, p[DISKMAP_QPAIR_POOL].nregion);

	/* Calculate the prp pool offset */
	*(ssize_t *)(uintptr_t)&qcb->prp_ofs =
		diskmap_obj_rel_offset(qcb, p[DISKMAP_PRPLIST_POOL].nregion);

	/* Set the number of queues */
	*(uint16_t *)(uintptr_t)&qcb->num_queues = num_queues;

	/* Set the number of qpairs+prp slots in this control block */
	*(uint16_t *)(uintptr_t)&qcb->num_slots = num_queues + qdepth;

	/* Set qdepth */
	*(uint16_t *)(uintptr_t)&qcb->qdepth = qdepth;

	/* Set qpair objsize */
	*(uint32_t *)(uintptr_t)&qcb->qp_obj_size = p[DISKMAP_QPAIR_POOL]._objsize;

	/* Set prp objsize */
	*(uint32_t *)(uintptr_t)&qcb->prp_obj_size = p[DISKMAP_PRPLIST_POOL]._objsize;

	/* Update the offset for the newly allocated object */
	if(ofs)
		*ofs = diskmap_qpaircb_abs_offset((void *)qcb);

	DMA_UNLOCK();

	return qcb;

fail_qpair:
	diskmap_qpaircb_free(qcb);
fail:
	DMA_UNLOCK();
	return NULL;
}

void
diskmap_qpaircb_destroy(struct diskmap_qpaircb *qcb, void *sq_obj, void *cq_obj)
{
	uint32_t i;
	struct diskmap_obj_pool *prp = &dm_mem.pools[DISKMAP_PRPLIST_POOL];

	DMA_LOCK();
	struct diskmap_slot *prpslot = &qcb->slot[qcb->num_queues];
	uint32_t qdepth = qcb->qdepth;


	/* Release the actual SQ, CQ objects first */
	if (sq_obj)
		diskmap_qpair_free(sq_obj);
	if (cq_obj)
		diskmap_qpair_free(cq_obj);
	for (i = 0; i < qdepth; i++)
		diskmap_obj_free(prp, prpslot[i].idx);
	if (qcb) {
		bzero(qcb, sizeof(*qcb));
		diskmap_qpaircb_free(qcb);
	}

	DMA_UNLOCK();
}


void
diskmap_dmar_config(struct diskmap_bufcb *bcb, bus_dma_tag_t tag, bus_dmamap_t
	*map)
{
	int err;
	struct diskmap_slot *slot;
	uint32_t i;

	DMA_LOCK();
	slot = bcb->slot;
	for (i = 0; i < bcb->num_slots; i++) {
		err = bus_dmamap_create(tag, 0, &map[i]);
		if (err) {
			D("Cannot create map @%u", i);
			;
		}
		bus_dmamap_load(tag, map[i], slot[i].addrinfo.vaddr, bcb->dr_buf_size,
			diskmap_dmar_load_map_cb, &slot[i].addrinfo.baddr, 0);
		if (err) {
			D("Cannot load map @%u", i);
			;
		} else {
			ND("Map loading -- address: %p, length: %u", slot[i].addrinfo.vaddr,
				bcb->dr_buf_size);
			;
		}
		if ( i == 0 || i == bcb->num_slots - 1)
			D("vaddr: %p, paddr: 0x%lx", slot[i].addrinfo.vaddr,
				slot[i].addrinfo.baddr);
	}

	DMA_UNLOCK();
}

void
diskmap_dmar_reset(bus_dma_tag_t tag, bus_dmamap_t *map, uint32_t nmaps)
{
	uint32_t i;

	DMA_LOCK();
	for (i = 0; i < nmaps; i++) {
		bus_dmamap_unload(tag, map[i]);
		bus_dmamap_destroy(tag, map[i]);
	}
	DMA_UNLOCK();

}
