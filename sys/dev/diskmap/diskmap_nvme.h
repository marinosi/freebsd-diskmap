/*
 * Copyright (C) 2014 Ilias Marinos.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _DISKMAP_NVME_H_
#define _DISKMAP_NVME_H_

#include <dev/diskmap/diskmap_kern.h>

#if ! defined(DISKMAP_D)
#define DISKMAP_D(format, ...)						\
	do {							\
		struct timeval __xxts;				\
		microtime(&__xxts);				\
		printf("%03d.%06d [%4d] %-25s " format "\n",	\
		(int)__xxts.tv_sec % 1000, (int)__xxts.tv_usec,	\
		__LINE__, __FUNCTION__, ##__VA_ARGS__);		\
	} while (0)
#endif

#define DISKMAP_ENABLE(x)	(x = TRUE)
#define DISKMAP_DISABLE(x)	(x = FALSE)

static void nvme_sync(void *, uint8_t);
static void nvme_doorbell_info(void *);

static void
nvme_doorbell_info(void *arg)
{
	struct sync_args *sa = (struct sync_args *) arg;
	const struct nvme_qpair *qp = (struct nvme_qpair *) sa->drv_qpair_ref;

	DISKMAP_D("Qpair @%p", qp);
	sa->sq_tail = nvme_mmio_read_4(qp->ctrlr, doorbell[qp->id].sq_tdbl);
	sa->cq_head = nvme_mmio_read_4(qp->ctrlr, doorbell[qp->id].cq_hdbl);

	DISKMAP_D("HW SQ tail: %u, HW CQ head: %u", sa->sq_tail, sa->cq_head);

}

static void
nvme_sync(void *arg, uint8_t flags)
{
	struct sync_args *sargs = (struct sync_args *) arg;
	struct nvme_qpair *qp = (struct nvme_qpair *) sargs->drv_qpair_ref;
	uint32_t cur;

	//mtx_lock(&qp->lock);

	if(qp->ctrlr->is_failed) {
		DISKMAP_D("ctrlr has failed!");
		return;
	}

	//DISKMAP_D("qpair id: %u (@%p)", qp->id, qp);
	if (flags & DISKMAP_SQ_SYNC) {
		cur = sargs->sq_tail;
		wmb();
		nvme_mmio_write_4(qp->ctrlr, doorbell[qp->id].sq_tdbl, cur);
		//DISKMAP_D("Pushed %u to SQ doorbell", cur);
	}
	if (flags & DISKMAP_CQ_SYNC) {
		cur = sargs->cq_head;
		wmb();
		nvme_mmio_write_4(qp->ctrlr, doorbell[qp->id].cq_hdbl, cur);
		//DISKMAP_D("Pushed %u to CQ doorbell", cur);
	}

	//mtx_unlock(&qp->lock);
}

static inline void
nvme_diskmap_qpair_construct(struct nvme_qpair *qpair)
{
	int ret;
	struct hw_qpair_info info;

	/* Initialize info. */
	bzero(&info, sizeof(info));
	info.drv_qpair_ref = (void *) qpair;	/* keep a reference */
	info.dev_unit = device_get_unit(qpair->ctrlr->dev);
	info.id = qpair->id;
	info.num_entries = qpair->num_entries;
	info.sqptr = (void **) &qpair->cmd;
	info.cqptr = (void **) &qpair->cpl;
	info.sync = nvme_sync;
	info.doorbell_info = nvme_doorbell_info;
	info.sq_head = qpair->sq_head;
	info.sq_tail = qpair->sq_tail;
	info.cq_head = qpair->cq_head;
	info.phase = 1;	/* qpair->phase is not initialized yet */
	info.max_xfer_size = qpair->ctrlr->max_xfer_size;
	info.dma_tag = qpair->dma_tag;

	/*
	 * XXX IM: The following 2 lines are going to be executed twice if diskmap
	 * doesn't attach, but there is no problem with that.
	 * The reason we do that is because we try to make less intrusive changes to
	 * the driver.
	 */
	qpair->sq_tdbl_off = nvme_mmio_offsetof(doorbell[qpair->id].sq_tdbl);
	qpair->cq_hdbl_off = nvme_mmio_offsetof(doorbell[qpair->id].cq_hdbl);

	ret = diskmap_attach(&info);
	if (ret) {
		DISKMAP_D("diskmap not attaching to qpair with id (%u)", qpair->id);
		DISKMAP_DISABLE(qpair->diskmap_enabled);
		return;
	}

	DISKMAP_D("cmd: %p, cpl: %p", qpair->cmd, qpair->cpl);

	if (qpair->tag)
		bus_teardown_intr(qpair->ctrlr->dev, qpair->res, qpair->tag);
	if (qpair->res)
		bus_release_resource(qpair->ctrlr->dev, SYS_RES_IRQ,
		    rman_get_rid(qpair->res), qpair->res);

	bus_dmamap_create(qpair->dma_tag, 0, &qpair->cmd_dma_map);
	bus_dmamap_create(qpair->dma_tag, 0, &qpair->cpl_dma_map);


	bus_dmamap_load(qpair->dma_tag, qpair->cmd_dma_map,
	    qpair->cmd, qpair->num_entries * sizeof(struct nvme_command),
	    nvme_single_map, &qpair->cmd_bus_addr, 0);
	bus_dmamap_load(qpair->dma_tag, qpair->cpl_dma_map,
	    qpair->cpl, qpair->num_entries * sizeof(struct nvme_completion),
	    nvme_single_map, &qpair->cpl_bus_addr, 0);


	DISKMAP_ENABLE(qpair->diskmap_enabled);
	DISKMAP_D("(ATTACH) diskmap successfully attached to qpair %u", qpair->id);
}

static inline void
nvme_diskmap_qpair_destroy(struct nvme_qpair *qpair)
{
	diskmap_detach(device_get_unit(qpair->ctrlr->dev), qpair->id);
}

#endif /* _DISKMAP_NVME_H_ */
