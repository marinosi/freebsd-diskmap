/*
 * Copyright (C) 2014 Ilias Marinos.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h> /* prerequisite */
#include <sys/types.h>
#include <sys/module.h>
#include <sys/errno.h>
#include <sys/param.h>  /* defines used in kernel.h */
#include <sys/poll.h>  /* POLLIN, POLLOUT */
#include <sys/kernel.h> /* types used in module initialization */
#include <sys/conf.h>	/* DEV_MODULE */
#include <sys/endian.h>
#include <sys/ioccom.h>
#include <sys/queue.h>

#include <sys/rwlock.h>

#include <vm/vm.h>      /* vtophys */
#include <vm/pmap.h>    /* vtophys */
#include <vm/vm_param.h>
#include <vm/vm_object.h>
#include <vm/vm_page.h>
#include <vm/vm_pager.h>
#include <vm/uma.h>

#include <sys/malloc.h>
#include <sys/socket.h> /* sockaddrs */
#include <sys/selinfo.h>
#include <sys/sysctl.h>
#include <sys/sx.h>
#include <machine/bus.h>        /* bus_dmamap_* */

#include <dev/diskmap/diskmap_kern.h>
#include <dev/diskmap/diskmap_private.h>
#include <dev/diskmap/diskmap_mem.h>
#include <sys/diskmap.h>


MALLOC_DEFINE(M_DISKMAP, "diskmap", "diskmap memory map");

int diskmap_verbose;

/* Sysctls */
SYSCTL_DECL(_dev_diskmap);
SYSCTL_NODE(_dev, OID_AUTO, diskmap, CTLFLAG_RW, 0, "Diskmap args");
SYSCTL_INT(_dev_diskmap, OID_AUTO, verbose,
    CTLFLAG_RW, &diskmap_verbose, 0, "Verbose mode");

DMG_LOCK_T	diskmap_global_lock;

static struct cdev *diskmap_dev;
static struct diskmap_adapter *d_adapter;

/* forw declarations */
static void diskmap_dtor_devfs(void *data);
static struct diskmap_kern_qpair *find_diskmap_kern_qpair_locked(const struct
	diskmap_hw_controller *ctrlr, uint32_t qpair_id);
static struct diskmap_hw_controller *find_diskmap_hw_controller_locked(int
	dev_unit);

/*
 * In order to track whether pages are still mapped, we hook into
 * the standard cdev_pager and intercept the constructor and
 * destructor.
 */

struct diskmap_vm_handle_t {
	struct cdev 		*dev;
	struct diskmap_priv_d	*priv;
};


static int
diskmap_dev_pager_ctor(void *handle, vm_ooffset_t size, vm_prot_t prot,
    vm_ooffset_t foff, struct ucred *cred, u_short *color)
{
	struct diskmap_vm_handle_t *vmh = handle;

	if (diskmap_verbose)
		D("handle %p size %jd prot %d foff %jd",
			handle, (intmax_t)size, prot, (intmax_t)foff);
	dev_ref(vmh->dev);
	return (0);
}

static void
diskmap_dev_pager_dtor(void *handle)
{
	struct diskmap_vm_handle_t *vmh = handle;
	struct cdev *dev = vmh->dev;

	if (diskmap_verbose)
		D("handle %p", handle);
	D("[DEBUG] pager_dtor called");
	free(vmh, M_DEVBUF);
	dev_rel(dev);
}

static int
diskmap_dev_pager_fault(vm_object_t object, vm_ooffset_t offset,
	int prot, vm_page_t *mres)
{
	vm_paddr_t paddr;
	vm_page_t page;
	vm_memattr_t memattr;
	vm_pindex_t pidx;

	ND("object %p offset %jd prot %d mres %p",
			object, (intmax_t)offset, prot, mres);
	memattr = object->memattr;
	pidx = OFF_TO_IDX(offset);
	paddr = diskmap_mem_ofstophys(offset);
	if (paddr == 0)
		return (VM_PAGER_FAIL);

	if (((*mres)->flags & PG_FICTITIOUS) != 0) {
		/*
		 * If the passed in result page is a fake page, update it with
		 * the new physical address.
		 */
		page = *mres;
		vm_page_updatefake(page, paddr, memattr);
	} else {
		/*
		 * Replace the passed in reqpage page with our own fake page and
		 * free up the all of the original pages.
		 */
#ifndef VM_OBJECT_WUNLOCK	/* FreeBSD < 10.x */
#define VM_OBJECT_WUNLOCK VM_OBJECT_UNLOCK
#define VM_OBJECT_WLOCK	VM_OBJECT_LOCK
#endif /* VM_OBJECT_WUNLOCK */

		VM_OBJECT_WUNLOCK(object);
		page = vm_page_getfake(paddr, memattr);
		VM_OBJECT_WLOCK(object);
		vm_page_lock(*mres);
		vm_page_free(*mres);
		vm_page_unlock(*mres);
		*mres = page;
		vm_page_insert(page, object, pidx);
	}
	page->valid = VM_PAGE_BITS_ALL;
	return (VM_PAGER_OK);
}

static struct cdev_pager_ops diskmap_cdev_pager_ops = {
	.cdev_pg_ctor = diskmap_dev_pager_ctor,
	.cdev_pg_dtor = diskmap_dev_pager_dtor,
	.cdev_pg_fault = diskmap_dev_pager_fault,
};

static int
diskmap_open(struct cdev *dev, int oflags, int devtype, struct thread *td)
{
	struct diskmap_priv_d *priv;
	int error;

	/*D("priv size: %zu", sizeof(struct diskmap_priv_d));*/
	priv = malloc(sizeof(struct diskmap_priv_d), M_DEVBUF, M_NOWAIT | M_ZERO);
	if (priv == NULL)
		return (ENOMEM);
	D("(open) priv->dp_refcount: %lu", priv->dp_refcount);

	error = devfs_set_cdevpriv(priv, diskmap_dtor_devfs);
	if(error)
		return (error);

	/* XXX IM: check if we need to update refcount */
	/*priv->dp_refcount++;*/

	return (0);
}

/* called with DMG_LOCK *not* held */
static int
diskmap_get_memory(struct diskmap_priv_d *p)
{
	int error = 0;
	DMG_LOCK();
	if (p->dp_mref == NULL) {
		/*
		 * If this is not the first allocation, neither the scheme has changed
		 * diskmap_mem_allocate() will return
		 */
		error = diskmap_mem_allocate();
		if(!error) {
			p->dp_mref = &dm_mem;
			++p->dp_refcount;
		}
	}
	DMG_UNLOCK();
	return (error);
}

static int
diskmap_mmap_single(struct cdev *cdev, vm_ooffset_t *foff, vm_size_t objsize,
    vm_object_t *objp, int nprot)
{
	int error;
	struct diskmap_vm_handle_t *vmh;
	struct diskmap_priv_d *priv;
	vm_object_t obj;

	if (diskmap_verbose)
		D("cdev %p foff %jd size %jd objp %p prot %d", cdev,
		    (intmax_t )*foff, (intmax_t )objsize, objp, nprot);

	DMG_LOCK();
	vmh = malloc(sizeof(struct diskmap_vm_handle_t), M_DEVBUF,
			      M_NOWAIT | M_ZERO);
	if (vmh == NULL)
		return (ENOMEM);
	vmh->dev = cdev;

	error = devfs_get_cdevpriv((void**)&priv);
	if (error)
		goto err_unlock;
	vmh->priv = priv;
	DMG_UNLOCK();

	error = diskmap_get_memory(priv);
	if (error)
		goto err_deref;

	obj = cdev_pager_allocate(vmh, OBJT_DEVICE,
		&diskmap_cdev_pager_ops, objsize, nprot,
		*foff, NULL);
	if (obj == NULL) {
		D("cdev_pager_allocate failed");
		error = EINVAL;
		goto err_deref;
	}

	*objp = obj;
	return (0);

err_deref:
	DMG_LOCK();
	priv->dp_refcount--;
err_unlock:
	DMG_UNLOCK();
// err:
	free(vmh, M_DISKMAP);
	return (error);
}

static int
diskmap_ioctl(struct cdev *dev, u_long cmd, caddr_t data, int fflag, struct
    thread *td)
{
	struct diskmap_priv_d *priv = NULL;
	struct dmreq *dmr = (struct dmreq *) data;
	int error;
	u_int mflags;
	struct diskmap_bufcb *bufcb;
	struct diskmap_hw_controller *ctrlr;
	struct diskmap_kern_qpair *kqpaircb;
	struct sync_args sa;
	uint32_t nsize = 0;	/* Size of buffer pool in bytes */
	uint32_t nslots;
	size_t bufcb_ofs;
	char tmpname[MAXNAMELEN];
	uint8_t flags = 0;

	(void)dev;
	(void)fflag;

	/* Check diskmap API for certain commands */
	switch (cmd) {
	case DIOCGINFO:
	case DIOCREQBUFPOOL:
		if (dmr->dr_version != DISKMAP_API) {
			D("API mismatch: got %d need %d", dmr->dr_version, DISKMAP_API);
			return (EINVAL);
		}
	}

	error = devfs_get_cdevpriv((void **)&priv);
	if (error) {
		return (error == ENOENT ? ENXIO : error);
	}

	switch (cmd) {
	case DIOCGINFO:
		DMG_LOCK();
		error = diskmap_mem_get_info(&dmr->dr_memsize, &mflags);
		if(error) {
			D("Error -- cannot get diskmap mem info");
			goto unlock_out;
		}
		dmr->dr_offset = 0;
		DMG_UNLOCK();
		break;
	case DIOCREQBUFPOOL:
#if 0
		error = diskmap_get_memory(priv);	/* handles locking internally */
		if (error)
			break;
#endif

		DMG_LOCK();

		/* Check first if memory is allocated (and if not get out) */
		/* XXX IM: This is redundant if we do "diskmap_get_memory()" before */
		if (priv->dp_mref == NULL) {
			D("Cannot request bufpool -- unallocated memory");
			error = ENOMEM;
			goto unlock_out;
		}

		if (diskmap_verbose)
			D("Allocating diskmap buffer pool");

		/* Get the size & name of the pool that is requested */
		nsize = dmr->dr_reqsz;
		dmr->dr_name[MAXNAMELEN-1] = '\0';	/* Null terminate */
		strncpy(tmpname, dmr->dr_name, MAXNAMELEN);

		/* Allocate a new unbound buffer pool */
		bufcb = diskmap_bufcb_new(nsize, tmpname, &bufcb_ofs, &nslots);
		if( bufcb == NULL) {
			error = ENOMEM;	/* Could also be "EEXIST" */
			goto unlock_out;
		}

		/* Let's get the return values */
		dmr->dr_memsize = diskmap_get_totalmem();
		dmr->dr_offset = bufcb_ofs;
		dmr->dr_nslots = nslots;
		if(diskmap_verbose)
			D("New bufcb object -- real addr: %p, offset: %zu", bufcb,
				bufcb_ofs);

		DMG_UNLOCK();
		break;
	case DIOCDELBUFPOOL:
		DMG_LOCK();
		/* If memory is not allocated at all get out */
		if (priv->dp_mref == NULL) {
			D("Cannot DELBUFPOOL -- unallocated memory");
			goto unlock_out;
		}

		dmr->dr_name[MAXNAMELEN-1] = '\0';	/* Null terminate */

		if (diskmap_verbose)
			D("Releasing diskmap buffer pool [%s]", dmr->dr_name);

		diskmap_bufcb_destroy(dmr->dr_name);
		DMG_UNLOCK();
		break;
	case DIOCREGQPAIRCB:
		error = diskmap_get_memory(priv);	/* handles locking internally */
		if (error)
			break;

		DMG_LOCK();
		/* XXX IM: This is redundant if we do "diskmap_get_memory()" before */
		if (priv->dp_mref == NULL) {
			D("Cannot attach on qpaircb -- unallocated memory");
			error = ENOMEM;
			goto unlock_out;
		}

		ctrlr = find_diskmap_hw_controller_locked(dmr->dr_devid);
		if (ctrlr == NULL) {
			D("Controller %d does not exist.", dmr->dr_devid);
			error = EINVAL;
			goto unlock_out;
		}

		/* locate the requested kern qpaircb */
		kqpaircb = find_diskmap_kern_qpair_locked(ctrlr, dmr->dr_qid);
		if (kqpaircb == NULL) {
			D("Qpaircb with id %u does not exist", dmr->dr_qid);
			error = EINVAL;	/* XXX IM: check error code */
			goto unlock_out;
		}

		/* XXX IM: check if the queue is already used and skip if so */

		/* Allocate a buffer pool */
		nsize = dmr->dr_reqsz;
		snprintf(tmpname, sizeof(tmpname), "nvme%d-%hu", dmr->dr_devid,
			(uint16_t) dmr->dr_qid);
		bufcb = diskmap_bufcb_new(nsize, tmpname, &bufcb_ofs, &nslots);
		if( bufcb == NULL) {
			error = ENOMEM;
			goto unlock_out;
		}

		/* Allocate space for the dma maps and setup DMAR hw */
		kqpaircb->payload_dma_map = malloc(nslots * sizeof(bus_dmamap_t),
			M_DISKMAP, M_NOWAIT | M_ZERO);
		if (kqpaircb->payload_dma_map == NULL) {
			error = ENOMEM;
			diskmap_bufcb_destroy1(bufcb);
			goto unlock_out;
		}
		kqpaircb->nmaps = nslots;

		diskmap_dmar_config(bufcb, kqpaircb->dma_tag, kqpaircb->payload_dma_map);

		/* update the doorbell info */
		bzero(&sa, sizeof(struct sync_args));
		sa.drv_qpair_ref = *(void **)(uintptr_t) &kqpaircb->drv_qpair_ref;
		kqpaircb->doorbell_info(&sa);
		kqpaircb->qcb->sq_tail = kqpaircb->qcb->sq_head = sa.sq_tail;
		kqpaircb->qcb->cq_head = sa.cq_head;
		kqpaircb->bcb = bufcb;

		/* Associate kern qpair with this fd */
		priv->dp_kqpaircb = kqpaircb;

		/* if qpaircb exists return all the required info */
		/*dmr->dr_nslots = kqpaircb->num_slots;*/
		dmr->dr_qdepth = kqpaircb->qdepth;
		dmr->dr_offset = kqpaircb->qcb_ofs;
		dmr->dr_qsize = kqpaircb->qp_obj_size;

		dmr->dr_offset1 = bufcb_ofs;
		dmr->dr_nslots = nslots;

		DMG_UNLOCK();
		break;
	case DIOCUNREGQPAIRCB:
		DMG_LOCK();
		if (priv->dp_mref == NULL || priv->dp_kqpaircb == NULL ||
			priv->dp_kqpaircb->bcb == NULL) {
			error = ENOMEM;
			goto unlock_out;
		}

		kqpaircb = *(struct diskmap_kern_qpair **)(uintptr_t)&priv->dp_kqpaircb;
		D("Unregister qpair with id: %u", kqpaircb->qid);
		diskmap_dmar_reset(kqpaircb->dma_tag, kqpaircb->payload_dma_map,
			kqpaircb->nmaps);
		free(kqpaircb->payload_dma_map, M_DISKMAP);
		kqpaircb->nmaps = 0;
		diskmap_bufcb_destroy1(kqpaircb->bcb);

		DMG_UNLOCK();
		break;
	case DIOCSQSYNC:
	case DIOCCQSYNC:
		/* FAST PATH */
		kqpaircb = *(struct diskmap_kern_qpair **)(uintptr_t)&priv->dp_kqpaircb;
		if (kqpaircb == NULL) {
			error = ENXIO;	/* get out if fd not registered to a queue */
			goto unlock_out;
		}

		rmb();

		ND("Qpair id: %u", kqpaircb->qid);
		if (cmd == DIOCSQSYNC) {
			flags |= DISKMAP_SQ_SYNC;
			sa.sq_tail = kqpaircb->qcb->sq_tail;	/* XXX: sanity checks? */
		} else {
			flags |= DISKMAP_CQ_SYNC;
			sa.cq_head = kqpaircb->qcb->cq_head;	/* XXX: sanity checks? */
		}

		/* Get the reference to the driver qpair */
		sa.drv_qpair_ref = *(void **)(uintptr_t) &kqpaircb->drv_qpair_ref;

		/* GO */
		kqpaircb->sync(&sa, flags);
		break;
	default:
		error = ENODEV;
	}

	return (error);

unlock_out:
	DMG_UNLOCK();
	return (error);
}

static int
diskmap_close(struct cdev *dev, int fflag, int devtype, struct thread *td)
{
	if (diskmap_verbose)
		D("dev %p fflag 0x%x devtype %d td %p", dev, fflag, devtype, td);
	return (0);
}

static struct cdevsw diskmap_cdevsw = {
	.d_version = D_VERSION,
	.d_name = "diskmap",
	.d_open = diskmap_open,
	/*.d_mmap = diskmap_mmap,*/
	.d_mmap_single = diskmap_mmap_single,
	.d_ioctl = diskmap_ioctl,
	/*.d_poll = diskmap_poll,*/
	.d_close = diskmap_close,
};

static void
diskmap_dtor_devfs(void *data)
{
	struct diskmap_priv_d *priv = data;

	DMG_LOCK();
	D("[DEBUG] diskmap_dtor_devfs called");
	if (priv->dp_refcount > 0) {
		D("priv->dp_refcount: %lu", priv->dp_refcount);
		diskmap_mem_deref();
		/*
		 * XXX IM: perhaps priv->dp_refcount = 0
		 * Investigate this, by following diskmap_get_memory(). In particular,
		 * when we open diskmap dev multiple times, we don't mmap() but we bump
		 * the refcount.
		 */
		--priv->dp_refcount;
	}
	bzero(priv, sizeof(*priv));	/* for safety */
	free(priv, M_DEVBUF);
	DMG_UNLOCK();
}

/*
 * should be called with lock held.
 */
static struct diskmap_hw_controller *
find_diskmap_hw_controller_locked(int dev_unit)
{
	struct diskmap_hw_controller *ctrlr;

	TAILQ_FOREACH(ctrlr, &d_adapter->ctrlr_q, ctrlrs) {
		if (ctrlr->dev_unit != dev_unit)
			continue;

		/* found */
		return ctrlr;
	}

	/* not found */
	return NULL;
}

/*
 * should be called with lock held.
 */
static struct diskmap_kern_qpair *
find_diskmap_kern_qpair_locked(const struct diskmap_hw_controller *ctrlr, uint32_t
	qpair_id)
{
	struct diskmap_kern_qpair *qp;

	TAILQ_FOREACH(qp, &ctrlr->qpair_q, qpairs) {
		if (qp->qid != qpair_id)
			continue;

		/* found */
		return qp;
	}

	/* not found */
	return NULL;
}

static struct diskmap_kern_qpair *
alloc_diskmap_kern_qpair_locked(struct diskmap_hw_controller *ctrlr, struct
	hw_qpair_info *info)
{
	uint32_t i;
	struct diskmap_kern_qpair *kqpr;


	kqpr = malloc(sizeof(*kqpr), M_DISKMAP, M_NOWAIT | M_ZERO);
	if (kqpr == NULL)
		return (NULL);	/* fail */

	*(uint32_t *)(uintptr_t)&kqpr->qid = info->id;
	*(uint16_t *)(uintptr_t)&kqpr->num_slots = 2;	/* hardcoded to two queues */
	*(uint32_t *)(uintptr_t)&kqpr->qdepth = info->num_entries;
	kqpr->sync = info->sync;	/* get the sync callback */
	kqpr->doorbell_info = info->doorbell_info;	/* get the doorbell info cb */
	kqpr->hw_sq_head = info->sq_head;
	kqpr->hw_sq_tail = info->sq_tail;
	kqpr->hw_cq_head = info->cq_head;
	kqpr->dma_tag = info->dma_tag;
	*(void **)(uintptr_t)&kqpr->drv_qpair_ref = info->drv_qpair_ref;
	*(uint32_t *)(uintptr_t)&kqpr->max_xfer_size = info->max_xfer_size;

	kqpr->prp_dma_map = malloc(kqpr->qdepth * sizeof(bus_dmamap_t), M_DISKMAP,
		M_NOWAIT | M_ZERO);
	if (kqpr->prp_dma_map == NULL) {
		free(kqpr, M_DISKMAP);
		return (NULL);
	}

	/* Create prp mappings */
	for (i = 0; i < kqpr->qdepth; i++)
		bus_dmamap_create(kqpr->dma_tag, 0, &kqpr->prp_dma_map[i]);

	kqpr->d_ctrlr = ctrlr;
	TAILQ_INSERT_TAIL(&ctrlr->qpair_q, kqpr, qpairs);
	return kqpr;
}

static void
destroy_diskmap_kern_qpair_locked(struct diskmap_kern_qpair *kqpr)
{
	uint32_t i;

	if (kqpr == NULL)
		return;

	for (i = 0; i < kqpr->qdepth; i++) {
		bus_dmamap_unload(kqpr->dma_tag, kqpr->prp_dma_map[i]);
		bus_dmamap_destroy(kqpr->dma_tag, kqpr->prp_dma_map[i]);
	}
	if (kqpr->qcb != NULL)
		diskmap_qpaircb_destroy(kqpr->qcb, kqpr->squeue, kqpr->cqueue);
	free(kqpr->prp_dma_map, M_DISKMAP);
	free(kqpr, M_DISKMAP);
}

static struct diskmap_hw_controller *
alloc_diskmap_hw_controller_locked(int dev_unit)
{
	struct diskmap_hw_controller *ctrlr;

	ctrlr = malloc(sizeof(*ctrlr), M_DISKMAP, M_NOWAIT | M_ZERO);
	if (ctrlr == NULL)
		return (NULL);	/* fail */

	ctrlr->dev_unit = dev_unit;
	TAILQ_INIT(&ctrlr->qpair_q);
	TAILQ_INSERT_TAIL(&d_adapter->ctrlr_q, ctrlr, ctrlrs);
	return ctrlr;
}

static void
destroy_diskmap_hw_controller_locked(struct diskmap_hw_controller *ctrlr)
{
	struct diskmap_kern_qpair *kqpr, *tmp;

	/* Iterate through any qpairs of this controller and destroy them */
	TAILQ_FOREACH_SAFE(kqpr, &ctrlr->qpair_q, qpairs, tmp) {
		D("(DETACH) Releasing resources of qpair %u", kqpr->qid);
		TAILQ_REMOVE(&ctrlr->qpair_q, kqpr, qpairs);
		destroy_diskmap_kern_qpair_locked(kqpr);
	}

	free(ctrlr, M_DISKMAP);
}

/*
 * Basic attach function to intercept IO qpair construction at nvme devices
 */
int
diskmap_attach(struct hw_qpair_info *info)
{
	int error;
	struct diskmap_hw_controller *ctrlr;
	struct diskmap_kern_qpair *kqpr;
	struct diskmap_qpaircb *qpaircb;
	size_t ofs;

	/*
	 * XXX IM: I am not sure if this can happen, since nvme module depends on
	 * diskmap, so I'd expect diskmap_init() to return successfully, before nvme
	 * can be loaded (and diskmap_attach() called later at some point)
	 */
	if(!diskmap_dev) {
		D("diskmap device is not initialized, do not attach");
		return (-1);
	}

	/* Check if this is an admin qpair and skip -- we won't attach here */
	if (info->id == 0) {
		D("(ATTACH) This is an admin qpair -- skip...");
		return (-1);
	}

	DMG_LOCK();
	/* Check first if memory is allocated (and if not allocate it) */
	error = diskmap_mem_allocate();
	if(!error) {
		if (d_adapter->dp_mref == NULL)
			d_adapter->dp_mref = &dm_mem;
	} else {
		goto unlock_out;
	}

	ctrlr = find_diskmap_hw_controller_locked(info->dev_unit);
	if (ctrlr == NULL) {
		/* Attaching to a new controller. */
		D("(ATTACH) New controller: %d.", info->dev_unit);
		ctrlr = alloc_diskmap_hw_controller_locked(info->dev_unit);
		if (ctrlr == NULL) {
			error = ENOMEM;
			goto unlock_out;
		}
	}

	kqpr = find_diskmap_kern_qpair_locked(ctrlr, info->id);
	if (kqpr) {
		D("(ATTACH) Qpair with id (%u) already exists -- out", info->id);
		error = -1;	/* XXX IM: find out the appropriate code */
		goto unlock_out;
	}

	kqpr = alloc_diskmap_kern_qpair_locked(ctrlr, info);
	if (kqpr == NULL) {
		D("(ATTACH) failed to allocate a new diskmap_kern_qpair");
		error = ENOMEM;
		goto unlock_out;
	}

	/* XXX */
	/* XXX IM DON'T forget to bump the refcount of dm_mem since we now use it */
	/* XXX Not needed anymore since we invoke diskmap_mem_allocate() */
	qpaircb = diskmap_qpaircb_new(kqpr, info->id, info->num_entries, &ofs);
	kqpr->qcb = qpaircb;
	if (qpaircb == NULL) {
		error = ENOMEM;
		destroy_diskmap_kern_qpair_locked(kqpr);
		goto unlock_out;
	}

	/* Update info for the new kqpr */
	*(size_t *)(uintptr_t)&kqpr->qcb_ofs = ofs;

	/* Update head, tail for SQ and CQ in the qpaircb */
	qpaircb->sq_head = kqpr->hw_sq_head;
	qpaircb->sq_tail = kqpr->hw_sq_tail;
	qpaircb->cq_head = kqpr->hw_cq_head;
	*(uint32_t *)(uintptr_t)&qpaircb->max_xfer_size = kqpr->max_xfer_size;
	qpaircb->qp_phase = info->phase;

	D("Driver qpair reference @%p", kqpr->drv_qpair_ref);
	/* Now we need to update the sqptr and cqptr for the nvme device */
	if(info->sqptr)
		*info->sqptr = kqpr->squeue;
	if(info->cqptr)
		*info->cqptr = kqpr->cqueue;

	DMG_UNLOCK();

	return (0);

unlock_out:
	DMG_UNLOCK();
	return (error);
}

void
diskmap_detach(int dev_unit, uint32_t qpair_id)
{
	struct diskmap_hw_controller *ctrlr;
	struct diskmap_kern_qpair *kqpr, *tmp;

	/* Skip quickly further processing in case of admin qpairs. */
	if (qpair_id == 0) {
		D("(DETACH) This is an admin qpair -- skip...");
		return;
	}

	DMG_LOCK();

	TAILQ_FOREACH(ctrlr, &d_adapter->ctrlr_q, ctrlrs) {
		if (ctrlr->dev_unit != dev_unit)
			continue;
		/* Find the appropriate qpair and destroy it. */
		TAILQ_FOREACH_SAFE(kqpr, &ctrlr->qpair_q, qpairs, tmp) {
			if (kqpr->qid != qpair_id)
				continue;
			D("(DETACH) Releasing resources of qpair %u", qpair_id);

			TAILQ_REMOVE(&ctrlr->qpair_q, kqpr, qpairs);
			destroy_diskmap_kern_qpair_locked(kqpr);
		}
	}

	DMG_UNLOCK();
}

static void
diskmap_terminate(void)
{
	struct diskmap_hw_controller *ctrlr, *tmp;

	DMG_LOCK();
	if (diskmap_dev)
		destroy_dev(diskmap_dev);

	/* Iterate through the active controllers and destroy them. */
	TAILQ_FOREACH_SAFE(ctrlr, &d_adapter->ctrlr_q, ctrlrs, tmp) {
		TAILQ_REMOVE(&d_adapter->ctrlr_q, ctrlr, ctrlrs);
		destroy_diskmap_hw_controller_locked(ctrlr);
	}

	/* Release diskmap adapter */
	free(d_adapter, M_DISKMAP);

	/* Release diskmap memory */
	diskmap_mem_terminate();

	DMG_UNLOCK();
	DMG_LOCK_DESTROY();
	wmb();
	printf("diskmap: unloaded module.\n");
}

int
diskmap_init(void)
{
	int ret;

	/* Initialize locks */
	DMG_LOCK_INIT();
	ret = diskmap_mem_init();
	if (ret != 0)
		goto fail;

	d_adapter = malloc(sizeof(struct diskmap_adapter), M_DISKMAP, M_NOWAIT
		| M_ZERO);
	if (!d_adapter)
		goto fail;
	TAILQ_INIT(&d_adapter->ctrlr_q);

	diskmap_dev = make_dev(&diskmap_cdevsw, 0, UID_ROOT, GID_WHEEL, 0660,
			      "diskmap");
	if(!diskmap_dev)
		goto fail;

	printf("diskmap: loaded module.\n");
	return (0);
fail:
	diskmap_terminate();
	return (EINVAL);
}

static int
diskmap_loader(__unused struct module *m, int what, __unused void *arg)
{

	int error = 0;

	switch (what) {
	case MOD_LOAD:
		error = diskmap_init();
		break;

	case MOD_UNLOAD:
		diskmap_terminate();
		break;

	default:
		error = EOPNOTSUPP;
		break;
	}

	return (error);
}

DEV_MODULE(diskmap, diskmap_loader, NULL);
MODULE_VERSION(diskmap, 1);
