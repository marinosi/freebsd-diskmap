/*
 * Copyright (C) 2014 Ilias Marinos.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _DISKMAP_PRIVATE_H_
#define _DISKMAP_PRIVATE_H_

#define	DM_LOCK_T	struct mtx
#define	DM_RWLOCK_T	struct rwlock
//#define	DM_SELINFO_T	struct selinfo

#ifndef DEV_DISKMAP
#define DEV_DISKMAP
#endif

#define ND(format, ...)
#define D(format, ...)						\
	do {							\
		struct timeval __xxts;				\
		microtime(&__xxts);				\
		printf("%03d.%06d [%4d] %-25s " format "\n",	\
		(int)__xxts.tv_sec % 1000, (int)__xxts.tv_usec,	\
		__LINE__, __FUNCTION__, ##__VA_ARGS__);		\
	} while (0)


MALLOC_DECLARE(M_DISKMAP);

/* Locking */
#define	DM_LOCK_T	struct mtx
#define	DMG_LOCK_T	struct sx
extern DMG_LOCK_T	diskmap_global_lock;

#define DMG_LOCK_INIT()	sx_init(&diskmap_global_lock, \
				"diskmap global lock")
#define DMG_LOCK_DESTROY()	sx_destroy(&diskmap_global_lock)
#define DMG_LOCK()	sx_xlock(&diskmap_global_lock)
#define DMG_UNLOCK()	sx_xunlock(&diskmap_global_lock)
#define DMG_LOCK_ASSERT()	sx_assert(&diskmap_global_lock, SA_XLOCKED)

#define DM_LOCK_INIT(x, y)	mtx_init(&(x), y, NULL, MTX_DEF)
#define DM_LOCK_DESTROY(x)	mtx_destroy(&(x))
#define DM_LOCK(x)	mtx_lock(&(x))
#define DM_UNLOCK(x)	mtx_unlock(&(x))

/* For debugging purposes */
extern int diskmap_verbose;

struct diskmap_kern_qpair {
	TAILQ_ENTRY(diskmap_kern_qpair) qpairs;

	/* XXX IM: Add a lock here */
	struct diskmap_hw_controller *d_ctrlr;
	const void		*drv_qpair_ref;
	const uint32_t	qid;
	const uint32_t	qdepth;
	const uint16_t	num_slots;	/* Number of queues */
	const uint32_t	qp_obj_size;	/* qpair objsize */
	const uint32_t	prp_obj_size;	/* qpair objsize */
	const uint32_t	max_xfer_size;	/* qpair objsize */

	uint16_t	used;
	/* HW head/tail indexes of sq/cq doorbells */
	uint32_t	hw_sq_head;
	uint32_t	hw_sq_tail;
	uint32_t	hw_cq_head;

	/* cache of userspace values */
	uint32_t	us_sq_head;
	uint32_t	us_sq_tail;
	uint32_t	us_cq_head;

	bus_dma_tag_t	dma_tag;
	bus_dmamap_t	*payload_dma_map;
	bus_dmamap_t	*prp_dma_map;

	uint32_t		nmaps;

	const size_t	qcb_ofs;	/* Offset of qcb to expose to userspace */
	struct diskmap_qpaircb	*qcb;

	const size_t	bcb_ofs;
	struct diskmap_bufcb	*bcb;
	void *squeue;		/* SQ vaddr */
	void *cqueue;		/* CQ vaddr */

	/* Callback for operations {w,r}sync and doorbell info */
	void		(*sync)(void *arg, uint8_t flags);
	void		(*doorbell_info)(void *);
} __aligned(CACHE_LINE_SIZE);

struct diskmap_hw_controller {
	/* Add caps */
	//DM_LOCK_T ctrlr_mtx;
	int		dev_unit;
	TAILQ_ENTRY(diskmap_hw_controller)	ctrlrs;
	TAILQ_HEAD(,diskmap_kern_qpair)	qpair_q;
};

struct diskmap_adapter {
	/* this supports at the moment only a single nvme controller */
	TAILQ_HEAD(, diskmap_hw_controller)		ctrlr_q;
	struct diskmap_mem_d			*dp_mref; /* ref to dm_mem */
};

struct diskmap_priv_d {
	struct diskmap_kern_qpair volatile *dp_kqpaircb;
	struct diskmap_mem_d	*dp_mref;	/* Ref to diskmap_mem_d */
	/*
	 * XXX IM: Add fields to reference the appropriate device adapter and the
	 * qpairs required
	 */
	unsigned long			dp_refcount;	/* use with NMA_LOCK held */
};

/* Function prototypes */
int diskmap_init(void);

#endif /* _DISKMAP_PRIVATE_H_ */
