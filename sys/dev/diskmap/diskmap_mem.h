/*
 * Copyright (C) 2014 Ilias Marinos.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _DISKMAP_MEM_H_
#define _DISKMAP_MEM_H_


extern struct diskmap_mem_d dm_mem;


/* Helper function for alignments */
static inline size_t
m_align(size_t s, size_t b)
{
	size_t i;

	i = (s & (b - 1));
	if (i) {
		ND("XXX aligning object by %d bytes", b - i);
		s += b - i;
	}
	return s;
}

/*
 * Round 16bit unsigned integers to next power of 2
 * (This gives us a max 32768)
 */
static inline uint16_t
round2np2(uint16_t x)
{
	x--;
	x |= x >> 1;
	x |= x >> 2;
	x |= x >> 4;
	x |= x >> 8;
	x++;

	return x;
}

/* Function prototypes */
int diskmap_mem_allocate(void);	/* Takes care of locking */
void diskmap_mem_deref(void);	/* Takes care of locking */
int diskmap_mem_init(void);	/* Takes care of locking */
int diskmap_mem_get_info(size_t *, u_int *);
void diskmap_mem_terminate(void);	/* Takes care of locking */
vm_paddr_t diskmap_mem_ofstophys(vm_ooffset_t offset);
void *diskmap_bufcb_new(u_int , const char *, size_t *, uint32_t *);
size_t diskmap_get_totalmem(void);
void diskmap_bufcb_destroy(const char *);
void diskmap_bufcb_destroy1(struct diskmap_bufcb *);
struct diskmap_qpaircb *diskmap_qpaircb_new(struct diskmap_kern_qpair *, uint32_t
	, uint32_t ,  size_t *);
void diskmap_qpaircb_destroy(struct diskmap_qpaircb *, void *, void *);
void diskmap_dmar_config(struct diskmap_bufcb *, bus_dma_tag_t ,
	bus_dmamap_t *);
void diskmap_dmar_reset(bus_dma_tag_t , bus_dmamap_t *, uint32_t );

#endif /* _DISKMAP_MEM_H_ */
